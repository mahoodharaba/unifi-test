import axios from "axios";
import { toast } from "react-toastify";

const AxiosInstance = () => {
  const axiosInstance = axios.create({
    baseURL: "https://bikeindex.org/api",
    options: false,
  });

  axiosInstance.interceptors.request.use(
    (config) => {
      config.headers["Accept"] = "application/json";

      if (config.method === "options") {
        config.headers["Access-Control-Max-Age"] = "3600";
      }

      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  axiosInstance.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (error.response) {
        toast.error(
          error.response.data.message ||
            error.response.data.error ||
            "Request failed"
        );
      } else {
        toast.error(`Please check your connection`);
      }

      return Promise.reject(error);
    }
  );
  return axiosInstance;
};

export default AxiosInstance;
