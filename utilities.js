export function convertDate(timeStamp) {
  const date = new Date(1000 * timeStamp);
  const ReadableDate = date.toDateString();
  return ReadableDate;
}

export function filterByDate(array, startDate, endDate, dataHeader) {
  let unixStartDate;
  let unixEndDate;
  let newData = [];
  if (startDate && endDate) {
    unixStartDate = Math.floor(new Date(startDate).getTime() / 1000);
    unixEndDate = Math.floor(new Date(endDate).getTime() / 1000);
    newData = array.filter(
      (data) =>
        data[dataHeader] >= unixStartDate && data[dataHeader] <= unixEndDate
    );
    return newData;
  } else if (startDate && !endDate) {
    unixStartDate = Math.floor(new Date(startDate).getTime() / 1000);
    newData = array.filter((data) => data[dataHeader] >= unixStartDate);
    return newData;
  } else if (!startDate && endDate) {
    unixEndDate = Math.floor(new Date(endDate).getTime() / 1000);
    newData = array.filter((data) => data[dataHeader] <= unixEndDate);
    return newData;
  }
}
