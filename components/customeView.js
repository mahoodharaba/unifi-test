import Bike from "./bikeComponent";

export default function PaginatedView({ rows, setPage, currentPage }) {

  function scroll(id) {
    document.querySelector(`#${id}`).scrollIntoView({ behavior: "smooth" });
  }


  return (
    <div id="top" className="w-full flex flex-col justify-start items-center space-y-4 pt-3">
      {rows && rows.length > 0 ? (
        rows.map((row, i) => {
          return <Bike key={`${row.id} - ${i}`} bike={row} />;
        })
      ) : (
        <p className="w-full text-center py-5 ">
          There are no results for your quota.
        </p>
      )}
      <div className="w-full flex flex-row-reverse md:justify-start justify-center items-center ">
        <div className="flex justify-center items-center space-x-2 select-none">
          <button
            onClick={() => {
              setPage(currentPage - 1);
              scroll("top");
            }}
            className={`bg-skin-primary text-skin-text border-skin-secondary px-2 py-1 rounded-lg hover:opacity-75 ${
              currentPage === 1 && `pointer-events-none cursor-not-allowed opacity-55`
            }`}
          >
            {"Previous Page"}
          </button>
          <p className="px-2 py-1 border-b border-skin-primary text-skin-text" >{currentPage ?? ""}</p>
          <button
            onClick={() => {
              setPage(currentPage + 1);
              scroll("top");
            }}
            className={`bg-skin-primary text-skin-text border-skin-secondary px-2 py-1 rounded-lg hover:opacity-75 ${
              currentPage === 5000 && `pointer-events-none cursor-not-allowed`
            }`}
          >
            {"Next Page"}
          </button>
        </div>
      </div>
    </div>
  );
}
