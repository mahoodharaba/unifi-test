import Image from "next/image";
import placeholder from "@/public/images/bike.png";
import { Tooltip } from "@nextui-org/react";
import Link from "next/link";
import { convertDate } from "@/utilities";

export default function Bike({ bike }) {
  return (
    <div className=" text-skin-text border border-skin rounded-lg lg:w-[90%] w-[80%] mx-auto flex lg:flex-row flex-col justify-around lg:items-center items-start p-2">
      <div className="lg:w-[15%] w-full flex justify-center items-center">
        <Tooltip
          className="bg-skin-primary m-2 rounded-lg  "
          content={
            <div className="w-full flex flex-col justify-center items-center space-y-3 text-skin-text ">
              <Image
                src={bike.thumb ?? placeholder}
                alt={bike.title ?? "bike image"}
                width={300}
                height={300}
                className="object-contain"
              />
              <div className="flex flex-col justify-center items-center space-y-1">
                <p className="font-medium">{bike.title}</p>
                <p>{bike.manufacturer_name}</p>
                <p>({bike.serial})</p>
              </div>
            </div>
          }
          placement="right-start"
        >
          <Image
            src={bike.thumb ?? placeholder}
            alt={bike.title ?? "bike image"}
            width={0}
            height={0}
            className="object-contain lg:w-full w-[70%] lg:h-[150px] h-auto "
          />
        </Tooltip>
      </div>
      <div className="lg:w-[80%] w-full flex flex-col justify-start lg:items-start items-center space-y-2">
        <Link href={`/bike/${bike.id}`} target="_blank">
          <strong className="underline decoration-skin-text hover:decoration-sky-600 hover:text-sky-600 transition-all duration-300 ease-in-out">
            {bike.title ?? ""}
          </strong>
        </Link>
        <div className="flex w-full  flex-col justify-start items-center">
          <label
            htmlFor="desc"
            className="mx-3 my-1 lg:min-w-[20%] w-full flex flex-wrap space-x-3"
          >
            <strong className="px-1">{"Description :"}</strong>
            <p id="desc">{bike.description ?? " - "}</p>
          </label>
          <label
            htmlFor="date"
            className="mx-3 my-1 w-full flex flex-wrap space-x-3"
          >
            <strong className="text-red-700 px-1">{"Date of theft :"}</strong>
            <p id="date">
              {bike.date_stolen ? convertDate(bike.date_stolen) : " - "}
            </p>
          </label>
          <label
            htmlFor="stolen"
            className="mx-3 my-1 w-full flex flex-wrap space-x-3"
          >
            <strong className="px-1">{"Status :"}</strong>
            <p
              id="stolen"
              className={`${
                bike.stolen == true ? ` text-red-500 ` : `text-green-500`
              }`}
            >
              {bike.stolen == true ? "Stolen" : "Retrieved"}
            </p>
          </label>
          <label
            htmlFor="location"
            className="mx-3 my-1 w-full flex flex-wrap space-x-3"
          >
            <strong className="px-1">{"Stolen from :"}</strong>
            <p id="location">{bike.stolen_location ?? " - "}</p>
          </label>
        </div>
      </div>
    </div>
  );
}
