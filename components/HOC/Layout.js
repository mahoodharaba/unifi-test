import Header from "../UI/Header";

const MainLayout = (WrappedComponent) => {
  const WithLayout = (props) => (
    <>
      <Header />
      <WrappedComponent {...props} />
    </>
  );
  WithLayout.displayName = `MainLayout(${getDisplayName(WrappedComponent)})`;

  return WithLayout;
};

const getDisplayName = (WrappedComponent) => {
  return WrappedComponent.displayName || WrappedComponent.name || "Component";
};

export default MainLayout;
