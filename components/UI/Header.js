import Link from "next/link";

export default function Header() {
  return (
    <header className="bg-skin-primary ">
      <nav className="w-[80%] mx-auto flex justify-between items-center py-3 ">
        <Link href={"/"}>
          <span>Duetschland Police Department</span>
        </Link>
        <ul>
          <li>
            <Link
              href={"/"}
              className="text-xl font-light border-b border-transparent hover:border-skin-text p-1 transition-all duration-[450ms] ease-in-out  text-skin-text "
            >
              Bikes Search
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}
