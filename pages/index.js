import MainLayout from "@/components/HOC/Layout";
import PaginatedView from "@/components/customeView";
import AxiosInstance from "@/interceptor";
import { filterByDate } from "@/utilities";
import { CalendarDate, DateInput } from "@nextui-org/react";
import { Ring } from "@uiball/loaders";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import { useQueries, useQuery } from "react-query";

function Home() {
  const Api = AxiosInstance();
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [searchQuery, setSearchQuery] = useState();
  const [searchInput, setSearchInput] = useState();
  const [stoleness, setStoleness] = useState("all");
  const [dateFiltering, setDateFiltering] = useState(false);
  const [openDate, setOpenDate] = useState(false);
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [invalid, setInvalid] = useState(false);
  const [filteredData, setFilteredData] = useState();
  const {
    data: bikes,
    isFetching: fetchingBikes,
    isLoading: loadingBikes,
    status: bikesStatus,
  } = useQuery(
    ["stolenBikes", page, perPage, searchQuery, stoleness],
    () => {
      return fetchStolenBikes(page, perPage, searchQuery, stoleness);
    },
    {
      staleTime: 1,
      refetchOnWindowFocus: false,
      refetchOnReconnect: true,
      keepPreviousData: true,
      onSuccess: (data) => {
        if (dateFiltering == true && (startDate || endDate)) {
          applyDateFilter(data.data.bikes);
        }
      },
    }
  );

  const {
    data: count,
    isFetching: fetchingCount,
    isLoading: loadingCount,
  } = useQuery(
    ["count", searchQuery, stoleness],
    () => {
      return fetchCount(searchQuery, stoleness);
    },
    {
      staleTime: 1,
      refetchOnWindowFocus: false,
      refetchOnReconnect: true,
      keepPreviousData: true,
    }
  );

  async function fetchStolenBikes(page, perPage, searchQuery, stoleness) {
    try {
      return await Api.get(`/v3/search`, {
        params: {
          page: page,
          per_page: perPage,
          stoleness: stoleness,
          query: searchQuery ?? null,
          Location: "Munich",
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  async function fetchCount(searchQuery, stoleness) {
    try {
      return await Api.get(`/v3/search/count`, {
        params: {
          Location: "Munich",
          stoleness: stoleness,
          query: searchQuery ?? null,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  function submitSearch(e) {
    e.preventDefault();
    setSearchQuery(searchInput);
    setPage(1);
  }

  function handlePageChange(pageNumber) {
    setPage(pageNumber);
  }

  function resetDateFilter() {
    setOpenDate(false);
    setDateFiltering(false);
    setFilteredData();
    setStartDate();
    setEndDate();
  }

  function applyDateFilter(bikes) {
    setDateFiltering(true);
    setFilteredData(
      filterByDate(
        bikes,
        startDate ?? null,
        endDate ?? null,
        "date_stolen"
      )
    );
  }

  return (
    <section className="lg:w-[80%] w-[90%] mx-auto border-x border-skin-primary my-10 px-5 flex flex-col justify-start items-center space-y-5">
      <div
        id="searchheader"
        className="w-full flex flex-col justify-start lg:items-start items-center space-y-3 lg:text-base text-sm "
      >
        <span className="text-2xl select-none lg:text-start text-center ">
          Search Quota for theft cases :
        </span>
        <form
          onSubmit={submitSearch}
          className="flex lg:justify-start justify-center items-center lg:text-base text-sm space-x-3 w-full"
        >
          <button
            type="submit"
            className="px-2 py-1 rounded-lg bg-skin-primary text-skin-text"
          >
            Search
          </button>
          <input
            className="bg-skin-text text-skin-secondary outline-none lg:w-[50%] w-max px-2 py-1 placeholder:lg:text-base placeholder:text-xs rounded-lg "
            placeholder="Search By the title of the case ( Full/Partial title ) "
            value={searchInput}
            onChange={(e) => {
              setSearchInput(e.target.value);
            }}
          />
        </form>
        <button
          className="bg-skin-secondary px-2 py-1 rounded-lg border border-skin-primary hover:opacity-75"
          onClick={() => {
            setOpenDate(true);
          }}
        >
          Filter by Date
        </button>
        {openDate == true && bikes && (
          <div className="w-full flex lg:flex-row flex-col justify-start items-center pb-3 border-b border-skin-primary ">
            <label
              className={`flex flex-col justify-start items-start space-y-2 px-2 py-2 m-2 border border-skin-primary rounded-lg ${
                filteredData && `pointer-events-none`
              } `}
            >
              <p>Starting Date :</p>
              <input
                type="date"
                defaultValue={new Date()}
                value={startDate}
                className="text-skin-text bg-transparent border-b border-skin-primary outline-none px-2 mx-2"
                onChange={(e) => {
                  setStartDate(e.target.value);
                }}
              />
            </label>
            <label
              className={`flex flex-col justify-start items-start space-y-2 px-2 py-2 m-2 border ${
                filteredData && `pointer-events-none`
              } ${
                invalid == true ? `border-red-500 ` : ` border-skin-primary `
              } rounded-lg`}
            >
              <p>Ending Date :</p>
              <input
                type="date"
                defaultValue={new Date()}
                value={endDate}
                className={`text-skin-text bg-transparent border-b outline-none px-2 mx-2 border-skin-primary `}
                onInvalid={() => {
                  console.log(`invalid`);
                }}
                onChange={(e) => {
                  if (e.target.value < startDate) {
                    setEndDate(e.target.value);
                    setInvalid(true);
                    return;
                  } else {
                    setInvalid(false);
                    setEndDate(e.target.value);
                  }
                }}
              />
              {invalid == true && (
                <p className="text-red-500 text-xs">
                  *The ending date should not be earlier than the starting date.
                </p>
              )}
            </label>
            <button
              disabled={
                invalid == true || (!startDate && !endDate) || filteredData
              }
              onClick={() => {applyDateFilter(bikes.data.bikes)}}
              className="px-3 py-1 bg-blue-700 transitio-all m-2 duration-500 disabled:bg-skin-primary disabled:pointer-events-none text-skin-text rounded-lg text-center h-max place-items-end"
            >
              Apply
            </button>
            <button
              onClick={resetDateFilter}
              className="px-3 py-1 bg-red-700 text-skin-text m-2 rounded-lg text-center h-max place-items-end"
            >
              Reset filter
            </button>
          </div>
        )}
      </div>
      <div
        className={`relative w-full h-full ${
          fetchingBikes == true || fetchingCount == true
            ? `opacity-65 pointer-events-none`
            : `opacity-100`
        }`}
      >
        {fetchingBikes == true && fetchingCount == true && (
          <div className="absolute top-0 left-0 bottom-0 right-0 flex justify-center items-center ">
            <Ring speed={3} lineWeight={5} color="white" size={55} />
          </div>
        )}
        {loadingBikes == true || loadingCount == true ? (
          <div className="w-full h-full flex justify-center items-center ">
            <Ring
              speed={3}
              lineWeight={5}
              color="white"
              size={55}
              className={``}
            />
          </div>
        ) : (
          <div className="w-full h-full">
            {count && (
              <div className="w-full h-[10%] border-2 border-skin-primary lg:text-base text-xs flex justify-stretch items-center rounded-lg overflow-hidden">
                <button
                  onClick={() => {
                    setStoleness("all");
                    setPage(1);
                  }}
                  className={` grow py-2 justify-center items-center border-x border-skin-primary ${
                    stoleness == "all"
                      ? `bg-skin-text text-skin-secondary pointer-events-auto`
                      : `bg-skin-secondary text-skin-text hover:bg-skin-primary`
                  }`}
                >
                  <p>{`All Cases in Munich`}</p>
                  <p>{`(${count.data.non + count.data.stolen})`}</p>
                </button>
                <button
                  onClick={() => {
                    setStoleness("stolen");
                    setPage(1);
                  }}
                  className={` grow py-2 border-x border-skin-primary ${
                    stoleness == "stolen"
                      ? `bg-skin-text text-skin-secondary pointer-events-auto`
                      : `bg-skin-secondary text-skin-text hover:bg-skin-primary`
                  }`}
                >
                  <p>{`Stolen within Munich`}</p>
                  <p>{`(${count.data.stolen})`}</p>
                </button>
                <button
                  onClick={() => {
                    setStoleness("non");
                    setPage(1);
                  }}
                  className={` grow py-2 border-x border-skin-primary ${
                    stoleness == "non"
                      ? `bg-skin-text text-skin-secondary pointer-events-auto`
                      : `bg-skin-secondary text-skin-text hover:bg-skin-primary`
                  }`}
                >
                  <p>{`Not Stolen`}</p>
                  <p>{`( ${count.data.non} )`}</p>
                </button>
              </div>
            )}

            {dateFiltering == true && filteredData && (
              <div className="text-center text-skin-text w-[90%] mx-auto flex justify-between items-center pt-3 ">
                {startDate && endDate && (
                  <p className="lg:text-xl text-sm ">{`Showing results between ( ${startDate} ) and ( ${endDate} ) :`}</p>
                )}
                {startDate && !endDate && (
                  <p className=" lg:text-xl text-sm ">{`Showing results on ( ${startDate} ) and after that :`}</p>
                )}
                {!startDate && endDate && (
                  <p className=" lg:text-xl text-sm ">{`Showing results on ( ${endDate} ) and before that :`}</p>
                )}
                <p className="lg:text-xl text-sm" >{`(${filteredData.length}) results `}</p>
              </div>
            )}

            {bikes && (
              <PaginatedView
                rows={
                  dateFiltering == true && filteredData
                    ? filteredData
                    : bikes.data.bikes
                }
                currentPage={page}
                setPage={(pageNumber) => {
                  handlePageChange(pageNumber);
                }}
              />
            )}
          </div>
        )}
      </div>
    </section>
  );
}

export default MainLayout(Home);
