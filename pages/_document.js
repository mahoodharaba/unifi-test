import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head/>
      <head>
        <title>
          Duetchsland P.D.
        </title>
      </head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
