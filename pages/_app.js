import "@/styles/globals.css";
import { QueryClient, QueryClientProvider } from "react-query";
import { ToastContainer } from "react-toastify";

  const queryClient = new QueryClient() ;

export default function App({ Component, pageProps }) {
  return (
    <>
      <QueryClientProvider client={queryClient} >
        <Component {...pageProps} />
        <ToastContainer
          theme="dark"
          position="top-right"
          draggable={true}
          autoClose={3000}
          closeOnClick={true}
          stacked
        />
      </QueryClientProvider>
    </>
  );
}
