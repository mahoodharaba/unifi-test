import MainLayout from "@/components/HOC/Layout";
import axios from "axios";
import Image from "next/image";
import placeholder from "@/public/images/bike.png";
import { convertDate } from "@/utilities";

export async function getServerSideProps(context) {
  const { params } = context;
  try {
    const response = await axios.get(
      `https://bikeindex.org/api/v3/bikes/${params.bikeId}`
    );
    return {
      props: {
        bike: response.data.bike,
      },
    };
  } catch (error) {
    return {
      notFound: true,
    };
  }
}

function BikePage({ bike }) {
  return (
    <div className="container mx-auto">
      <div className=" mx-auto w-[90%] bg-skin-secondary border-2 border-skin-primary rounded-lg  text-skin-text overflow-hidden shadow-lg m-10 p-8 flex flex-col justify-around items-center  ">
        <div className=" w-full flex lg:flex-row flex-col lg:justify-between justify-around items-start lg:text-base text-sm ">
          <div className="lg:w-[40%] w-full ">
            <Image
              src={bike.large_img ?? bike.thumb ?? placeholder}
              alt={bike.serial}
              width={0}
              height={0}
              className="object-contain w-full h-auto"
            />
          </div>
          <div className="lg:w-[55%] w-full flex flex-col justify-around h-full items-start space-y-5 ">
            <p className="text-3xl font-bold my-4 lg:text-start text-center ">{bike.title}</p>
            <strong className="mx-4" >Description :</strong>
            <p className=" mx-4 ">{bike.description ?? " - "}</p>
            <p>
              <span className="font-semibold m-4 py-2">Date of theft :</span>
              {bike.date_stolen ? convertDate(bike.date_stolen) : " - "}
            </p>
            <p>
              <span className="font-semibold m-4 py-4">Reported at :</span>
              {bike.registration_created_at
                ? convertDate(bike.registration_created_at)
                : " - "}
            </p>
            <p>
              <span className="font-semibold m-4 py-4">Serial:</span>
              {bike.serial}
            </p>
            <p>
              <span className="font-semibold m-4 py-4">Frame Model:</span>
              {bike.frame_model}
            </p>
            <p>
              <span className="font-semibold m-4 py-4">Manufacturer:</span>
              {bike.manufacturer_name}
            </p>
            <div className="flex justify-start items-center">
              <span className="font-semibold mx-4">Status:</span>
              <p
                id="stolen"
                className={`${
                  bike.stolen == true ? ` text-red-500 ` : `text-green-500`
                }`}
              >
                {bike.stolen == true ? "Stolen" : "Retrieved"}
              </p>
            </div>
            <p>
              <span className="font-semibold m-4 py-4">Colors:</span>
              {bike.frame_colors.join(", ")}
            </p>
            <p>
              <span className="font-semibold m-4 py-4">Location Found:</span>
              {bike.location_found}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainLayout(BikePage);
